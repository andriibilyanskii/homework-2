const Users = require('../../model/users');
const Credentials = require('../../model/credentials');
const Notes = require('../../model/notes');
const {errorObj} = require('../../model/error');
const {successObj} = require('../../model/success');

/**
 * @constructor
 * @param {string} req - request
 * @param {string} res - response
 */
async function deleteProfile(req, res) {
  try {
    if (!req.user.username) {
      throw Error();
    }

    const user = await Users.findOne({username: req.user.username});
    if (user) {
      const notes = await Notes.find({userId: req.user.user_id});
      if (notes) {
        await Notes.deleteMany({userId: req.user.user_id});
      }
      await Credentials.deleteOne({username: req.user.username});
      await Users.deleteOne({username: req.user.username});
      res.status(200).send(successObj);
    } else {
      res.status(400).send(errorObj);
    }
  } catch (e) {
    res.status(500).send(errorObj);
  }
}

module.exports = deleteProfile;
