const mongoose = require('mongoose');

const logsSchema = new mongoose.Schema({
  log: {
    type: Object,
    require: true,
    trim: true,
  },
});

const Logs = mongoose.model('Log', logsSchema);

module.exports = Logs;
