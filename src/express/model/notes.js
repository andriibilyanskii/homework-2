const mongoose = require('mongoose');

const notesSchema = new mongoose.Schema({
  userId: {
    type: String,
    require: true,
    trim: true,
  },
  completed: {
    type: Boolean,
    require: true,
  },
  text: {
    type: String,
    require: true,
    trim: true,
  },
  createdDate: {
    type: String,
    require: true,
    trim: true,
  },
});

const Notes = mongoose.model('Note', notesSchema);

module.exports = Notes;
