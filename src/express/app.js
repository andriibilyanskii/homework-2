const express = require('express');
require('../db/mongoose');
const notesRouter = require('./router/notes');
const authRouter = require('./router/auth');
const usersRouter = require('./router/users');
const cors = require('cors');
const validateToken = require('./middleware/authorization');
const createLog = require('./logs');
const app = express();
require('dotenv').config();

const port = process.env.PORT || 8080;
app.use(express.json());
app.use(cors());

app.use((req, res, next) => {
  createLog(req, res);
  res.setHeader('Content-Type', 'application/json');
  next();
});

app.use(authRouter);
app.use(validateToken);
app.use(notesRouter);
app.use(usersRouter);

app.listen(port);
