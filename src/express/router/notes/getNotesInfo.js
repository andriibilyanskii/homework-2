/**
 * @constructor
 * @param {Array} notes
 */
function getNotesInfo(notes) {
  return notes.map((e) => {
    return {
      _id: e._id,
      userId: e.userId,
      completed: e.completed,
      text: e.text,
      createdDate: e.createdDate,
    };
  });
}

module.exports = getNotesInfo;
