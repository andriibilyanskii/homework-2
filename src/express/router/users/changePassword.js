const {errorObj} = require('../../model/error');
const {successObj} = require('../../model/success');
const Credentials = require('../../model/credentials');
const comparePassword = require('../../../shared/comparePassword');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

/**
 * @constructor
 * @param {string} req - request
 * @param {string} res - response
 */
async function changePassword(req, res) {
  try {
    const {oldPassword, newPassword} = req.body;

    if (!oldPassword || !newPassword) {
      throw new Error();
    }

    const user = await Credentials.findOne({username: req.user.username});
    if (user) {
      if (comparePassword(oldPassword, user.password)) {
        const encryptedPassword = await bcrypt.hash(newPassword, 10);
        const token = jwt.sign(
            {
              username: user.username,
              password: newPassword,
            },
            process.env.TOKEN_KEY,
            {
              expiresIn: '2h',
            },
        );
        await Credentials.updateOne(
            {username: req.user.username},
            {$set: {password: encryptedPassword, token: token}},
        );
        res.status(200).send(successObj);
      } else {
        res.status(400).send(errorObj);
      }
    }
  } catch (e) {
    res.status(500).send(errorObj);
  }
}

module.exports = changePassword;
